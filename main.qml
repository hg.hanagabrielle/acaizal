﻿import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "Componentes"
import "Js/config.js" as Cf

ApplicationWindow {
    id: window
    visible: true
    width: Cf.width
    height: Cf.heigth
    minimumHeight: Cf.heigth / 2
    minimumWidth: Cf.width / 2
    title: qsTr("Meu Açaizal")

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight
        background: Rectangle {
            color: "#871F78"
            anchors.fill: parent
        }

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            color: "white"
            font.bold: true
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.5
        height: window.height
        background: Rectangle {
          gradient: Gradient {
                  GradientStop { position: 0.0; color: "lightsteelblue" }
                  GradientStop { position: 1.0; color: "#871F78" }
              }
       }
        Column {
            anchors.fill: parent
            Repeater {
                model: ListModel {
                    ListElement {
                        title: qsTr("Blocos")
                        url: "1Blocos.qml"
                    }
                    ListElement {
                        title: qsTr("Guia de Manejo")
                        url: "2Guia de Manejo.qml"
                    }
                    ListElement {
                        title: qsTr("Tutorial")
                        url: "3Tutorial.qml"
                    }
                    ListElement {
                        title: qsTr("Salvar Informações")
                        url: "4Perfil.qml"
                    }
                    ListElement {
                        title: qsTr("Contatos")
                        url: "5Contatos.qml"
                    }
                    ListElement {
                        title: qsTr("Sobre")
                        url: "6Sobre.qml"
                    }
                }
                delegate: ItemDelegate {
                    text: title
                    font.bold: true
                    width: parent.width
                    onClicked: {
                        stackView.push(url)
                        drawer.close()
                    }
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "0HomeForm.qml"
        anchors.fill: parent
    }
}
