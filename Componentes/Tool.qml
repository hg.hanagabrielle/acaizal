﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtMultimedia 5.12

import "../js/config.js" as Cf
import "../componentes"

ToolBar {
    property alias title: labelName.text
    property alias toolButtonMenu: buttonLeft.toolButtonMenu

    id: toolBarAPP
    padding: 10
    height: 60

    background: Rectangle {
        id: toolBarBg
        color: Cf.backgroundColor
    }

    contentItem: Row {
        anchors.fill: parent
        anchors.margins: 10

        ToolButton {
            property bool toolButtonMenu
            property string iconName: toolButtonMenu ? 'menu' : 'back'

            id: buttonLeft
            height: parent.height
            width: height
            icon.source: 'qrc:/static/icons/' + iconName + '.svg'
            icon.color: 'white'

            onClicked: {
                if (toolButtonMenu) {
                    drawer.open()
                } else {
                    stackView.pop()
                }
            }

            background: Rectangle {
                radius: 50
                color: "#80000000"
                opacity: (parent.pressed) ? 0.15 : 0
            }
        }

        Label {
            id: labelName
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            width: parent.width - (buttonLeft.width + buttonRigth.width)
            height: parent.height
            font.pixelSize: height * 0.4
            font.bold: true
            color: 'white'
            elide: Text.ElideRight
        }

        Rectangle {
            id: buttonRigth
            visible: streamRadio.playbackState === Audio.PlayingState
            height: parent.height
            width: height
            color: "transparent"
            clip: true
            Timer {
                id: timer
                interval: 200
                repeat: true
                running: parent.visible
                onTriggered: {
                    rowBar.height = rowBar.height !== parent.height
                            * 0.6 ? parent.height * 0.6 : parent.height * 0.4
                }
            }
            Row {
                id: rowBar
                anchors.fill: parent
                anchors.margins: 5
                anchors.bottomMargin: 10
                spacing: 3
                Repeater {
                    model: 3
                    width: parent.width
                    height: parent.height
                    delegate: Rectangle {
                        id: element
                        color: "white"
                        width: rowBar.width / 3 - 3
                        height: rowBar.height * Math.random()
                        anchors.bottom: parent.bottom
                        Behavior on height {
                            NumberAnimation {
                                duration: 200
                            }
                        }
                    }
                }
            }
        }
    }
}
