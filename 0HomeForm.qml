import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id: page
    visible: true
    transformOrigin: Item.Center
    contentWidth: 0
    font.capitalization: Font.AllUppercase
    font.wordSpacing: -0.4
    font.family: "Tahoma"
    spacing: -1
    property alias logoSource: logo.source



    Rectangle {
        color: "transparent"
        anchors.centerIn: parent
        width: logo.width
        height: logo.height + nomeLogo.height

        Image {
            id: logo
            width: 181
            height: 141
            z: 1
            fillMode: Image.PreserveAspectCrop
            source: "Imagens/images.jpg"
        }
            Text {
                id: nomeLogo
                anchors.top: logo.bottom
                width: 172
                height: 68
                color: "#712ddb"
                font.pointSize: 21
                text: qsTr("Meu Açaizal")
                font.underline: false
                font.italic: true
                fontSizeMode: Text.Fit
                font.wordSpacing: -0.1
                font.bold: true
                font.family: "Times New Roman"
                elide: Text.ElideRight
                renderType: Text.NativeRendering
                textFormat: Text.AutoText
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
    }

}




