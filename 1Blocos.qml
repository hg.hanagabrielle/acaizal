import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "Js/config.js" as Cf
import "Componentes"

PaginaApp {
    id: paginaApp

    title: qsTr("Blocos")

    Button {
        id: buttonAdd
        y: 432
        width: 48
        height: 40
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.horizontalCenter: parent.horizontalCenter3
        display: AbstractButton.IconOnly
        onClicked: popup.open()
        background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: control.down ? "#17a81a" : "#21be2b"
                border.width: 1
                radius: 2
            }
        Image {
            id: iconAdd
            height: parent.height
            anchors.bottomMargin: -2
            anchors.horizontalCenterOffset: 0
            anchors.leftMargin: -40
            width: parent.width
            fillMode: Image.PreserveAspectFit
            anchors.left: parent.right
            anchors.topMargin: -4
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            source: "qrc:/Imagens/button-add.png"
        }
       }

       Popup {
           id: popup
           x: 100
           y: 100
           width: 300
           height: 400
           modal: true
           focus: true
           contentItem: Rectangle {
                  Text {
                      id: textAviso
                      text: qsTr("Informação")
                      font.pixelSize: 17
                      font.family: "Times"
                      horizontalAlignment: Text.AlignHCenter
                      anchors.horizontalCenter: parent.horizontalCenter
                  }
                  Label {
                     id: labelAviso
                     x: 0
                     anchors.top: textAviso.bottom
                     width: parent.width
                     wrapMode: Label.Wrap
                     text: qsTr(" Antes de fazer qualquer atividade no açaizal, o proprietário deve procurar a instituição estadual competente e solicitar as informações necessárias para obtenção da autorização de limpeza ou de manejo do açaizal nativo.
A autorização é fornecida pela instituição competente, após o proprietário apresentar os documentos e demais informações consideradas necessárias pela instituição.")
                     anchors.topMargin: 15
                  }
                  Button {
                      x: 167
                      anchors.top: labelAviso.bottom
                      anchors.right: parent.right
                      text: "Ok, entendi."
                      font.preferShaping: false
                      hoverEnabled: false
                      anchors.rightMargin: 8
                      anchors.topMargin: 135
                      display: AbstractButton.TextOnly
                      onClicked: popup2.open()
                  }
                  ToolButton {
                      id: sair
                      x: 8
                      y: 327
                      text: qsTr("Sair")
                      anchors.top: control3
                      onClicked: popup.close()
                  }
           }

           closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
       }
       Popup {
           id: popup2
           x: 100
           y: 100
           width: 300
           height: 400
           modal: true
           focus: true
           contentItem: Rectangle {
               Text {
                   id: element
                   x: 8
                   y: 5
                   text: qsTr("Digite um nome para a sua área: *")
                   font.pixelSize: 12
               }

               TextArea {
                   id: control1
                   x: 8
                   y: 26
                   width: 261
                   height: 27
                   placeholderText: qsTr("Insira o nome")
                   anchors.top: element
                   background: Rectangle {
                       implicitWidth: 200
                       implicitHeight: 40
                       border.color: control1.enabled ? "#9932CC" : "transparent"
                   }
               }

               Text {
                   id: element1
                   x: 8
                   y: 59
                   text: qsTr("Comprimento: *")
                   font.pixelSize: 12
               }

                TextField {
                    id: control2
                    x: 8
                    y: 80
                    width: 261
                    height: 27
                    anchors.topMargin: 60
                    renderType: Text.QtRendering
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control2.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    placeholderText: qsTr("Insira o comprimento")
                    inputMethodHints: Qt.ImhDigitsOnly
                                validator: DoubleValidator {
                                    bottom: 0
                                }
                }

                Text {
                    id: element2
                    x: 8
                    y: 113
                    text: qsTr("Largura: *")
                    font.pixelSize: 12
                }

                TextField {
                    id: control3
                    x: 8
                    y: 134
                    width: 261
                    height: 27
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control3.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    placeholderText: qsTr("Insira a largura")
                    inputMethodHints: Qt.ImhDigitsOnly
                                validator: DoubleValidator {
                                    bottom: 0
                                }
                    renderType: Text.QtRendering
                    anchors.top: element
                }
                Text {
                    id: element3
                    x: 8
                    y: 167
                    text: qsTr("Tamanho da área em ha:")
                    font.pixelSize: 12
                }
                TextField {
                    x: 8
                    y: 188
                    width: 261
                    height: 29
                    background: Rectangle {
                        color: "transparent"
                        border.color: "#9932CC"
                        radius: 2
                        }
                    text: control2.text * control3.text
                    readOnly: true
                    inputMethodHints: Qt.ImhDigitsOnly
                    }

                Text {
                    id: element4
                    x: 8
                    y: 223
                    text: qsTr("Informação adicional (Opcional): ")
                    font.pixelSize: 12
                }
                TextArea {
                    id: control4
                    x: 8
                    y: 244
                    width: 261
                    height: 27
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control3.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    renderType: Text.QtRendering
                    anchors.top: element
                }
                ToolButton {
                    id: cancelar
                    x: 8
                    y: 327
                    text: qsTr("Cancelar")
                    anchors.top: control3
                    onClicked: popup2.close()
                }

                        ToolButton {
                            id: salvar
                            x: 196
                            y: 327
                            width: 71
                            height: 40
                            text: qsTr("Salvar")
                            anchors.top: control3
                        }



           }

           closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
      }
}
