import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "Js/config.js" as Cf
import "Componentes"

PaginaApp {
    title: qsTr("Contatos")

    Flickable {
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        contentHeight: 1100
        height: 1100
        antialiasing: false
        clip: true

        Text {
            id: text0
            x: 0
            y: 9
            text: "IBAMA"
            font.bold: true
            anchors.topMargin: 21
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#820cb3"
            wrapMode: Label.Wrap
        }

        Label {
            id: label
            x: 0
            text: "Site - http://www.ibama.gov.br/fale-com-o-ibama
Formulário de Solicitação de Auxílio - https://servicos.ibama.gov.br/ctf/formulario_solicitacao_auxilio.php"
            anchors.topMargin: 15
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
            anchors.top: text0.bottom
        }
        Text {
            id: text1
            x: 0
            text: "Unidades IBAMA Pará"
            font.bold: true
            anchors.topMargin: 21
            anchors.top: label.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#820cb3"
            wrapMode: Label.Wrap
        }
        Text {
            id: text2
            x: 0
             text: "Superintendência do Ibama no Pará (Supes/PA)"
             font.underline: true
            font.bold: false
            anchors.topMargin: 47
            anchors.top: label.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap

        }

        Label {
            id: label1
            x: 0
            anchors.top: text2.bottom
            text: "Artur Vallinoto Bastos
Endereço: Travessa Lomas Valentinas nº 907 - Pedreira - Cep: 66087-441 - Belém/PA
Telefone: (91) 3210-4700
Voip: 80(91)4700
E-mail: supes.pa@ibama.gov.br / gabinete.pa@ibama.gov.br / artur.bastos@ibama.gov.br"
            anchors.topMargin: 13
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text3
            x: 0
             text: "Divisão Técnico-Ambiental"
             font.underline: true
            font.bold: false
            anchors.topMargin: 15
            anchors.top: label1.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label2
            x: 0
            anchors.top: text3.bottom
            text: "Eduardo Charly de Araújo Lameira
Chefe de Divisão Técnico-Ambiental
Endereço: Travessa Lomas Valentinas nº 907 - Pedreira - Cep: 66087-441 - Belém/PA
e-mail: ditec.pa@ibama.gov.br / eduardo.lameira@ibama.gov.br"
            anchors.topMargin: 13
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text4
            x: 0
             text: "Divisão de Administração e Finanças"
             font.underline: true
            font.bold: false
            anchors.topMargin: 16
            anchors.top: label2.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label3
            x: 0
            anchors.top: text4.bottom
            text: "José Carlos Dantas e Silva
Chefe da Divisão de Administração e Finanças
Endereço: Travessa Lomas Valentinas nº 907 - Pedreira - Cep: 66087-441 - Belém/PA
e-mail: diaf.para@ibama.gov.br / jose-carlos.dantas-silva@ibama.gov.br"
            anchors.topMargin: 14
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text5
            x: 0
             text: "Gerência Executiva do Ibama em Marabá"
             font.underline: true
            font.bold: false
            anchors.topMargin: 23
            anchors.top: label3.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label4
            x: 0
            anchors.top: text5.bottom
            text: "Hildemberg da Silva Cruz
Gerente Executivo do Ibama em Marabá
Endereço: Rua Paraná, 459, Jardim Belo Horizonte - Cep: 68503-420 - Marabá/PA
Telefone: (94) 3324-2000 e 3324-6674
e-mail: gerencia-maraba.pa@ibama.gov.br / hildemberg.cruz@ibama.gov.br"
            anchors.topMargin: 14
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text6
             text: "Gerência Executiva do Ibama em Santarém"
             font.underline: true
            font.bold: false
            anchors.topMargin: 23
            anchors.top: label4.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label5
            anchors.top: text6.bottom
            text: "Maria Luiza Gonçalves de Souza
Gerente Executiva do Ibama em Santarém
Endereço: Avenida Tapajós, nº 2.267 - Laguinho - Cep: 68010-000 - Santarém/PA
Telefone: (93) 3522-3032
e-mail: gabinete.santarem.pa@ibama.gov.br / maria-luiza.souza@ibama.gov.br"
            anchors.topMargin: 14
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text7
             text: "Unidade Técnica de 1º Nível em Altamira"
             font.underline: true
            font.bold: false
            anchors.topMargin: 23
            anchors.top: label5.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label6
            anchors.top: text7.bottom
            text: "Roberto Fernandes Abreu
Chefe de Unidade Técnica de 1º Nível em Altamira
Endereço: Rua Coronel José Porfírio, s/nº, São Sebastião - Cep: 68370-000 - Altamira/PA
Telefone: (93) 3515-1748
e-mail: roberto.abreu@ibama.gov.br"
            anchors.topMargin: 14
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Text {
            id: text8
             text: "Centro de Triagem de Animais Silvestres (Cetas) no Pará"
             font.underline: true
            font.bold: false
            anchors.topMargin: 23
            anchors.top: label6.bottom
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            color: "#66068f"
            wrapMode: Label.Wrap
        }
        Label {
            id: label7
            anchors.top: text8.bottom
            text: "Cetas Belém
Endereço: Rua João Coelho, Benevides, Belém/PA. CEP: 68.795-000
Telefone: (91) 3210-4775"
            anchors.topMargin: 14
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
    }
}
