import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../Js/config.js" as Cf
import "../Componentes"

PaginaApp {

    title: qsTr("Como Demarcar")

    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 900
        antialiasing: false
        clip: true

        Text {
            id: demarcandoBlocos
            x: 0
            y: 13
            text: qsTr("Demarcando os Blocos")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Label {
            id: label1
            anchors.top: demarcandoBlocos.bottom
            text: qsTr("Deve-se realizar a demarcação de um bloco de 40 metros x 25 metros (1.000 metros quadrados).")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: demarcando1
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label1.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/demarcando.png"
        }
        Label {
            id: label2
            x: 0
            y: 266
            anchors.top: limpando.bottom
            text: qsTr(" A corda com 40 metros deve ser esticada no meio do bloco e, perpendicular a esta, são esticadas as cordas com 25 metros, uma em cada ponta e a outra no meio. ")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap

        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
