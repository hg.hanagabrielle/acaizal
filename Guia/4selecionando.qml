import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../Js/config.js" as Cf
import "../Componentes"

PaginaApp {

    title: qsTr("Como Selecionar")

    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 2000
        antialiasing: false
        clip: true

        Text {
            id: selecionando
            x: 0
            y: 13
            text: qsTr("Selecionando as árvores e palmeiras")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Label {
            id: label1
            anchors.top: selecionando.bottom
            text: qsTr("Nesta etapa, deve-se selecionar e manter no bloco as plantas com produto de maior valor econômico e cultural, seja madeira, frutos, fibras, látex ou de uso medicinal.
Deve-se, também, manter pelo menos uma planta de valor desconhecido de cada espécie para a manutenção da biodiversidade local.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: selecionando1
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label1.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/selecionando.png"
        }
        Label {
            id: label2
            anchors.top: selecionando1.bottom
            text: qsTr("Em cada bloco, além dos açaizeiros, devem ser mantidas 25 plantas bem distribuídas, sendo até 5 palmeiras de outras espécies (2 adultas e 3 jovens) e até 20 árvores (4 grossas, 4 médias e 12 finas). As demais plantas devem ser eliminadas.
Para a derrubada dos murumuruzeiros, deve-se primeiro cortar as folhas com uma foice de cabo grande para evitar acidentes e causar menor dano nos açaizeiros.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: selecionando2
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label2.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/selecionando2.png"
        }
        Label {
            id: label3
            anchors.top: selecionando2.bottom
            text: qsTr(" Relembrando que uma boa distribuição de árvores no açaizal é muito importante para aumentar a produção de frutos e reduzir o trabalho de limpeza.
A eliminação de plantas deve ser realizada pela derrubada das árvores finas e palmeiras e anelamento das árvores médias e grossas. O anelamento, uma técnica usada para a eliminação de árvores mais grossas, consiste na retirada da casca das árvores numa faixa que envolva toda a roda do tronco, numa largura variável, dependendo da espécie. No anelamento de árvores, é fácil observar que ocorre diferenças entre as espécies. Na faveira e no guajaraí, por exemplo, o efeito do anelamento é relativamente rápido. Entretanto, no taperebá
e na caxinguba, às vezes o anelamento não faz efeito.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: selecionando3
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label3.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/selecionando3.png"
        }
        Label {
            id: label4
            anchors.top: selecionando3.bottom
            text: qsTr(" Nas áreas manejadas, as árvores aneladas devem ser monitoradas. Caso o efeito desejado não seja atingido, é necessário refazer o anelamento até que o efeito seja observado.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Label {
            id: label5
            anchors.top: label4.bottom
            text: qsTr("Devem ser deixadas no bloco as 40 melhores touceiras e, em cada touceira, os 5 melhores açaizeiros adultos, os 4 melhores açaizeiros jovens e os 3 melhores perfilhos. Na seleção, devem ser cortadas as plantas muito altas, finas, tortas, e as de baixa produção de frutos. Açaizeiros muito altos apresentam baixa produção de frutos, dificultam a colheita e ainda podem causar acidentes. O palmito destas plantas deve ser aproveitado.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: selecionando4
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label5.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/selecionando4.png"
        }
        Label {
            id: label6
            anchors.top: selecionando4.bottom
            text: qsTr("Açaizeiros adultos são aqueles que estão produzindo frutos; açaizeiros jovens são aqueles com mais de 2 metros de altura que ainda não produziram frutos.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
    }
}
