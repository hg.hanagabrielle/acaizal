import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../Js/config.js" as Cf
import "../Componentes"

PaginaApp {

    title: qsTr("Manutenção do Açaizal")

    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 900
        antialiasing: false
        clip: true

        Text {
            id: manutencao
            x: 0
            y: 13
            text: qsTr("Manutenção do açaizal")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Label {
            id: label1
            anchors.top: manutencao.bottom
            text: qsTr("O açaizal deve ser mantido limpo por meio da roçagem das brotações de plantas de valor desconhecido, assim como receber limpezas periódicas das touceiras, mantendo-se 5 açaizeiros produtivos em cada uma.
Também devem ser deixadas rebrotações nas touceiras, em número suficiente, para substituir os açaizeiros adultos que alcancem uma altura que dificulte a colheita.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: manutencao1
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label1.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/manutencao.png"
        }
        Label {
            id: label2
            x: 0
            y: 266
            anchors.top: manutencao1.bottom
            text: qsTr("A cada 3 ou 4 anos, os açaizeiros com mais de 12 metros de altura devem ser cortados e o palmito aproveitado, com o objetivo de deixar os açaizeiros mais baixos e produtivos.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap

        }
        Image {
            id: manutencao2
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label2.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/manutencao2.png"
        }
    }
}
