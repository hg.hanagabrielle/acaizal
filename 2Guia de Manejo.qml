import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import QtMultimedia 5.12
import "Js/config.js" as Cf
import "Componentes"

PaginaApp {
    property string colorsTitles: Cf.backgroundColor.replace('#', '#e6')

    title: qsTr("Guia de Manejo")
    Column {
        width: parent.width
        anchors.fill: parent
        anchors.top: parent.top
        anchors.margins: 10
        Repeater {
            model: ListModel {
                ListElement {
                    title: qsTr("Como limpar o açaizal")
                    url: "qrc:/Guia/1limpar.qml"
                    sourceIcon: "qrc:/Imagens/limpar.png"
                }
                ListElement {
                    title: qsTr("Como demarcar")
                    url: "qrc:/Guia/2demarcar.qml"
                    sourceIcon: "qrc:/Imagens/Demarcar.svg"
                }
                ListElement {
                    title: qsTr("Como classificar")
                    url: "qrc:/Guia/3classificar.qml"
                    sourceIcon: "qrc:/Imagens/Classificar.svg"
                }
                ListElement {
                    title: qsTr("Como selecionar")
                    url: "qrc:/Guia/4selecionando.qml"
                    sourceIcon: "qrc:/Imagens/Selecionar.svg"
                }
                ListElement {
                    title: qsTr("Como plantar")
                    url: "qrc:/Guia/5plantar.qml"
                    sourceIcon: "qrc:/Imagens/Plantar.svg"
                }
                ListElement {
                    title: qsTr("Manutenção do açaizal")
                    url: "qrc:/Guia/6manutencao.qml"
                    sourceIcon: "qrc:/Imagens/Manutencao.svg"
                }
            }
            delegate: ItemDelegate {
                text: title
                font.bold: true
                width: parent.width
                icon {
                    source: sourceIcon
                    color: "#3CB371"
                    height: 30
                    width: 20
                }
                onClicked: {
                    stackView.push(url)
                    drawer.close()
                }
            }
        }
    }

    /*StackView {
        id: stackView
        initialItem: ".qml"
        anchors.fill: parent
    }*/
}
